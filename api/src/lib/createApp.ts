import express from 'express';
import cors from 'cors';
import compression from 'compression';
import passport from 'passport';

export function createApp() {
    const app = express();

    if (process.env.NODE_ENV === 'development') {
        app.use(cors());
    }

    app.use(compression());

    app.use(passport.initialize());

    return app;
}
