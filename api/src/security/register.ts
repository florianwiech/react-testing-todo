import { promisify } from 'util';
import { createToken } from './login';
import { User } from '../entity';
import { IContext } from '../types/types';
import { getConnection } from 'typeorm';

const bcrypt = require('bcrypt');
const hashAsync = promisify(bcrypt.hash);

export interface IUserCreate {
    email: string;
    password: string;
    firstName: string;
    lastName: string;
    [name: string]: any;
}

export async function register(
    userToCreate: IUserCreate,
    context: IContext = { connection: getConnection() },
) {
    const { email: userInputEmail, password, firstName, lastName } = userToCreate;
    const email = userInputEmail.toLowerCase();

    const userRepository = context.connection.getRepository(User);
    const user = await userRepository.findOne({ email });

    // email already exists
    if (user) {
        throw Error('Email already exists');
    }

    // hash password and create user
    const hash = await hashAsync(password, 10);

    const createdUser = userRepository.create({
        email,
        password: hash,
        firstName,
        lastName,
    });

    const savedUser = await userRepository.save(createdUser);

    return {
        user: savedUser,
        token: createToken(savedUser),
    };
}
