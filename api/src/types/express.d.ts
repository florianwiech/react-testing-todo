import * as core from 'express-serve-static-core';
import { IObject } from './types';

declare namespace e {
    interface Request extends core.Request {
        user: IObject;
    }
}
