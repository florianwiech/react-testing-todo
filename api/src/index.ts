import { NextFunction, Request, Response } from 'express';

require('dotenv').config();
import 'reflect-metadata';
import bodyParser from 'body-parser';
import { createConnection } from 'typeorm';
import { createApp } from './lib/createApp';
import { User } from './entity';
import './security/authentication/jwtStrategy';
import * as path from 'path';
import todoRouter from './routes/todo/index';
import { createAuthentication, authRouter } from './security/authentication';

import cors from 'cors';

function expressErrorHandler(err: any, req: Request, res: Response, next: NextFunction) {
    if (err! instanceof Error) {
        next();
    }
    console.error(`${new Date().getTime()} ${err}`);

    if (err.error && err.error.isJoi) {
        // we had a joi error, let's return a custom 400 json response
        res.status(400).json({
            type: err.type,
            detail: err.error.toString(),
            statusCode: 400,
        });
    } else {
        res.status(err.status || 500).json({
            detail: err.message,
            name: err.name,
            statusCode: err.status,
        });
    }
}

async function main() {
    const app = createApp();

    const connection = await createConnection({
        type: 'postgres',
        host: process.env.TYPEORM_HOST,
        username: process.env.TYPEORM_USERNAME,
        password: process.env.TYPEORM_PASSWORD,
        database: process.env.TYPEORM_DATABASE,
        entities: [path.join(__dirname, 'entity/**')],
    });

    1;
    app.use(bodyParser.json());
    app.use(cors());
    app.use(authRouter());
    app.use('/api/todos', createAuthentication(app), todoRouter);
    app.use(expressErrorHandler);
    const server = app.listen(process.env.PORT, () => {
        const address = server.address();
        if (typeof address === 'object') {
            const { address: host, port } = address;
            console.log('🚀 Listening on %s:%s', host, port);
        }
    });
}

main().catch(e => {
    console.error(e);
    process.exit(1);
});
