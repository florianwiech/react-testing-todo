// @flow

import React from 'react'

import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom'
import { createStore, applyMiddleware, type Store } from 'redux'
import { Provider } from 'react-redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk'
import { Row, Col, Layout } from 'antd'

import NotificationsSystem from 'reapop'
import theme from 'reapop-theme-wybo'

import { TodoListPage } from './pages/TodoListPage'
import { LoginPage } from './pages/LoginPage'
import { RegisterPage } from './pages/RegisterPage'
import { isAuthenticated } from './auth'

import { mainReducer as reducer } from './store/reducers'
import { urls } from './urls'

const initialState = {
  todos: [],
}

const store: Store = createStore(
  reducer,
  initialState || { todos: [], loading: false, error: null },
  composeWithDevTools(applyMiddleware(thunk)),
)
const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      isAuthenticated() === true ? (
        <Component {...props} />
      ) : (
        <Redirect to="/auth/login" />
      )
    }
  />
)
export const App = () => (
  <Provider store={store}>
    <BrowserRouter>
      <>
        <NotificationsSystem theme={theme} />
        <Layout style={{ height: '100vh' }}>
          <Layout.Header>
            <h1 style={{ color: 'whitesmoke' }}>Do It!</h1>
          </Layout.Header>
          <Layout.Content>
            <Row>
              <Col xs={2} sm={4} md={6} lg={8} xl={8} />
              <Col xs={20} sm={16} md={12} lg={8} xl={8}>
                <Switch>
                  <Route
                    exact
                    path={urls.app.auth.login}
                    component={LoginPage}
                  />
                  <Route
                    exact
                    path={urls.app.auth.register}
                    component={RegisterPage}
                  />
                  <PrivateRoute exact path="/" component={TodoListPage} />
                </Switch>
              </Col>
              <Col xs={2} sm={4} md={6} lg={8} xl={8} />
            </Row>
          </Layout.Content>
        </Layout>
      </>
    </BrowserRouter>
  </Provider>
)
