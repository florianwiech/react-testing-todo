// @flow

export const isAuthenticated = () => !!localStorage.getItem('auth')

export const getToken = () => localStorage.getItem('auth')

export const authenticate = async (token: string) => {
  localStorage.setItem('auth', token)
}

export const logout = () => {
  localStorage.removeItem('auth')
}
