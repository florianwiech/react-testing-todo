// @flow

export interface IEntity {
  id: string;
}

export interface ITodo extends IEntity {
  id: string;
  title: string;
  done: boolean;
}

export class Todo implements ITodo {
  id: string = `${Math.random()}`

  title: string

  done: boolean

  // needed for ApolloLinkState
  constructor(title: string, done: boolean = false) {
    this.title = title
    this.done = done
  }
}
