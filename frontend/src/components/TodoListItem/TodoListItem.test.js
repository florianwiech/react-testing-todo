import React from 'react'
import { waitForElement } from 'react-testing-library'
import { render } from '../../test-utils'
import { TodoListItem } from './index'
import { Todo } from '../../types'

describe('TodoListItem', () => {
  it('renders with default props', () => {
    render(<TodoListItem item={new Todo('test')} />)
  })

  it('shows Input field on edit press', async () => {
    // Arrange
    const { getByTestId, getByLabelText } = render(
      <TodoListItem item={new Todo('test')} />,
    )

    // Act
    getByTestId('edit-todo').click()
    const inputNode = await waitForElement(() => getByLabelText(/edit value/i))

    // Assert
    expect(inputNode.getAttribute('type')).toBe('text')
    expect(inputNode.getAttribute('value')).toBe('test')
  })
})
