import React from 'react'
import { render } from '../../test-utils'
import { FormInput } from './index'

describe('FormInput', () => {
  it('renders with default props', () => {
    render(
      <FormInput
        name="email"
        placeholder="Your email"
        label="email"
        values={{ email: '' }}
        errors={{ email: null }}
      />,
    )
  })
})
