// @flow

import React from 'react'
import { Redirect } from 'react-router'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { Formik } from 'formik'
import { Form, Button } from 'antd'
import { FormInput } from '../../components/FormInput/index'

import * as AuthActions from '../../store/actions/Auth'
import { urls } from '../../urls'

type Props = {
  login: ({ email: string, password: string }) => void,
  authenticated: boolean,
}

type LoginCredentials = {
  email: string,
  password: string,
}

class LoginPageComponent extends React.Component<Props> {
  handleSubmit = async (formikResponse: LoginCredentials) => {
    await this.props.login(formikResponse)
  }

  render() {
    if (this.props.authenticated) {
      return <Redirect to="/" />
    }
    return (
      <>
        <Formik
          initialValues={{ email: '', password: '' }}
          onSubmit={this.handleSubmit}
        >
          {({ handleSubmit, ...rest }) => (
            <Form onSubmit={handleSubmit}>
              <FormInput
                name="email"
                placeholder="Your email"
                label="email"
                {...rest}
              />
              <FormInput
                name="password"
                placeholder="Your password"
                label="password"
                type="password"
                {...rest}
              />

              <Form.Item>
                <Button type="primary" htmlType="submit">
                  Login
                </Button>
              </Form.Item>
            </Form>
          )}
        </Formik>
        <Link to={urls.app.auth.register}>Register</Link>
      </>
    )
  }
}

const mapStateToProps = ({ auth }) => ({
  authenticated: auth.authenticated,
})

const mapDispatchToProps = dispatch => ({
  login: ({ email, password }: LoginCredentials) => {
    dispatch(AuthActions.login(email, password))
  },
})

export const LoginPage = connect(
  mapStateToProps,
  mapDispatchToProps,
)(LoginPageComponent)
