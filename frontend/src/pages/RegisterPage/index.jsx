// @flow

import React from 'react'
import { Redirect } from 'react-router'
import { connect } from 'react-redux'

import { Formik } from 'formik'
import { Form, Button } from 'antd'
import { FormInput } from '../../components/FormInput/index'

import * as AuthActions from '../../store/actions/Auth'

type Props = {
  register: ({
    email: string,
    password: string,
    firstName: string,
    lastName: string,
  }) => void,
  authenticated: boolean,
}
class RegisterPageComponent extends React.Component<Props> {
  handleSubmit = async formikResponse => {
    await this.props.register(formikResponse)
  }

  render() {
    if (this.props.authenticated) {
      return <Redirect to="/" />
    }
    return (
      <Formik
        initialValues={{ email: '', password: '', firstName: '', lastName: '' }}
        onSubmit={this.handleSubmit}
      >
        {({ handleSubmit, ...rest }) => (
          <Form onSubmit={handleSubmit}>
            <FormInput
              name="firstName"
              placeholder="Your firstname"
              label="Firstname"
              {...rest}
            />
            <FormInput
              name="lastName"
              placeholder="Your lastname"
              label="Lastname"
              {...rest}
            />
            <FormInput
              name="email"
              placeholder="Your email"
              label="Email"
              {...rest}
            />
            <FormInput
              name="password"
              placeholder="Your password"
              label="Password"
              type="password"
              {...rest}
            />

            <Form.Item>
              <Button type="primary" htmlType="submit">
                Register
              </Button>
            </Form.Item>
          </Form>
        )}
      </Formik>
    )
  }
}

const mapStateToProps = state => ({
  authenticated: state.auth.authenticated,
})

const mapDispatchToProps = dispatch => ({
  register: ({ email, password, lastName, firstName }) => {
    dispatch(AuthActions.register(email, password, firstName, lastName))
  },
})

export const RegisterPage = connect(
  mapStateToProps,
  mapDispatchToProps,
)(RegisterPageComponent)
