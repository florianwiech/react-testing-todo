// @flow

import axios from 'axios'
import { getToken } from './auth'

export const instance = () =>
  axios.create({
    baseURL: '/',
    timeout: 1000,
    headers: { Authorization: `Bearer ${getToken() || ''}` },
  })
