// @flow
import { type Dispatch } from 'redux'
import { notify } from 'reapop'
import { urls } from '../../urls'
import { instance } from '../../axios'

export const actions = {
  CREATE_TODO_START: 'CREATE_TODO_START',
  CREATE_TODO_SUCCESS: 'CREATE_TODO_SUCCESS',
  CREATE_TODO_ERROR: 'CREATE_TODO_ERROR',
  UPDATE_TODO_START: 'UPDATE_TODO_START',
  UPDATE_TODO_SUCCESS: 'UPDATE_TODO_SUCCESS',
  UPDATE_TODO_ERROR: 'UPDATE_TODO_ERROR',
  REMOVE_TODO_START: 'REMOVE_TODO_START',
  REMOVE_TODO_SUCCESS: 'REMOVE_TODO_SUCCESS',
  REMOVE_TODO_ERROR: 'REMOVE_TODO_ERROR',
  FETCH_TODO_START: 'FETCH_TODO_START',
  FETCH_TODO_SUCCESS: 'FETCH_TODO_SUCCESS',
  FETCH_TODO_ERROR: 'FETCH_TODO_ERROR',
}

export const getTodos = () => async (dispatch: Dispatch) => {
  dispatch({
    type: actions.FETCH_TODO_START,
  })
  instance()
    .get(urls.api.todos)
    .then(todos => {
      dispatch({
        type: actions.FETCH_TODO_SUCCESS,
        payload: todos.data,
      })
    })
    .catch(e => {
      dispatch({
        type: actions.FETCH_TODO_ERROR,
        error: {
          msg: 'Unexpected Error',
          payload: e,
        },
      })
      const { response } = e
      dispatch(
        notify({ message: response.data.detail, status: response.status }),
      )
    })
}

export const createTodoItem = (title: string) => async (dispatch: Dispatch) => {
  dispatch({
    type: actions.CREATE_TODO_START,
  })
  instance()
    .post(urls.api.todos, { title })
    .then(todos => {
      dispatch({
        type: actions.CREATE_TODO_SUCCESS,
        payload: todos.data,
      })
    })
    .catch(e => {
      dispatch({
        type: actions.CREATE_TODO_ERROR,
        error: {
          msg: 'Unexpected Error',
          payload: e,
        },
      })

      const { response } = e
      dispatch(
        notify({ message: response.data.detail, status: response.status }),
      )
    })
}

export const updateTodoItem = (todoId: string, todo: Object) => async (
  dispatch: Dispatch,
) => {
  dispatch({
    type: actions.UPDATE_TODO_START,
  })
  const requestObj = { title: todo.title, done: todo.done }
  instance()
    .put(urls.api.todo(todoId), requestObj)
    .then(todos => {
      dispatch({
        type: actions.UPDATE_TODO_SUCCESS,
        payload: { data: todos.data, id: todoId },
      })
    })
    .catch(e => {
      dispatch({
        type: actions.UPDATE_TODO_ERROR,
        error: {
          msg: 'Unexpected Error',
          payload: e,
        },
      })
      const { response } = e
      dispatch(
        notify({ message: response.data.detail, status: response.status }),
      )
    })
}

export const removeTodoItem = (todoId: string) => async (
  dispatch: Dispatch,
) => {
  dispatch({
    type: actions.REMOVE_TODO_START,
  })
  instance()
    .delete(urls.api.todo(todoId))
    .then(() => {
      dispatch({
        type: actions.REMOVE_TODO_SUCCESS,
        payload: {
          todoId,
        },
      })
    })
    .catch(e => {
      dispatch({
        type: actions.REMOVE_TODO_ERROR,
        error: {
          msg: 'Unexpected Error',
          payload: e,
        },
      })

      const { response } = e
      dispatch(
        notify({ message: response.data.detail, status: response.status }),
      )
    })
}
