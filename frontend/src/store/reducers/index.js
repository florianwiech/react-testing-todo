// @flow

import { combineReducers, type Reducer, type Action } from 'redux'
import { reducer as notificationsReducer } from 'reapop'

import { isAuthenticated } from '../../auth'
import { actions as TodoActions } from '../actions/Todo'
import { actions as AuthActions } from '../actions/Auth'

import * as helper from '../helper'

const itemReducer = (state = [], action: Action) => {
  switch (action.type) {
    case TodoActions.FETCH_TODO_SUCCESS:
      return action.payload
    case TodoActions.CREATE_TODO_SUCCESS:
      return helper.create(state, action.payload)
    case TodoActions.UPDATE_TODO_SUCCESS:
      return helper.update(state, action.payload.id, action.payload.data)
    case TodoActions.REMOVE_TODO_SUCCESS:
      return helper.remove(state, action.payload.todoId)
    default:
      return state
  }
}

const authenticated = (state = isAuthenticated(), action) => {
  switch (action.type) {
    case AuthActions.AUTH_REGISTER_SUCCESS:
    case AuthActions.AUTH_LOGIN_SUCCESS:
      return true
    case AuthActions.AUTH_LOGOUT_SUCCESS:
    case AuthActions.AUTH_LOGOUT_ERROR:
    case AuthActions.AUTH_LOGIN_ERROR:
      return false
    default:
      return state
  }
}

const auth: Reducer = combineReducers({
  authenticated,
})

export const mainReducer: Reducer = combineReducers({
  notifications: notificationsReducer(),
  todos: itemReducer,
  auth,
})
