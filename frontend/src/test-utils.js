// @flow

import React from 'react'
import { render } from 'react-testing-library'
import NotificationsSystem from 'reapop'
import { BrowserRouter } from 'react-router-dom'
import theme from 'reapop-theme-wybo'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import { mainReducer as reducer } from './store/reducers'

type RenderOptions = {
  initialState: Object,
}

const customRender = (node, { initialState }: RenderOptions = {}) => {
  const store = createStore(reducer, initialState || {}, applyMiddleware(thunk))
  return render(
    <Provider store={store}>
      <BrowserRouter>
        <>
          <NotificationsSystem theme={theme} />
          {node}
        </>
      </BrowserRouter>
    </Provider>,
  )
}

// re-export everythingo-library'
export * from 'react-testing-library'

// override render method
export { customRender as render }
