module.exports = {
  webpack: (initialConfig, env) => {
    // eslint-disable-next-line
    const config = require('react-app-rewire-css-modules-extensionless').webpack(
      initialConfig,
      env,
      {
        /* options */
      },
    )
    // The line below is equivalent
    // config = require('react-app-rewire-css-modules-extensionless')(config, env, { /* options */ });

    // You may apply other rewires as well

    return config
  },
  jest: initialConfig => {
    // eslint-disable-next-line
    const config = require('react-app-rewire-css-modules-extensionless').jest(
      initialConfig,
    )

    // You may apply other rewires as well

    return config
  },
}
