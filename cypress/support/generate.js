import { build, fake } from "test-data-bot";

export const userBuilder = build("User").fields({
  firstName: fake(f => f.name.firstName()),
  lastName: fake(f => f.name.lastName()),
  email: fake(f => f.internet.email()),
  password: fake(f => f.internet.password())
});

export const todoBuilder = build("Todo").fields({
  title: fake(f => f.lorem.sentence()),
  done: false
});
