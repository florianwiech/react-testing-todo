import "cypress-testing-library/add-commands";

describe("Login", () => {
  it("is redirected if not authenticated", () => {
    cy.visit("/");
    cy.url().should("eq", `${Cypress.config().baseUrl}/auth/login`);
  });
  it("can redirect to register", () => {
    cy.visit("/");
    cy.getByText(/register/i).click();
    cy.url().should("eq", `${Cypress.config().baseUrl}/auth/register`);
  });
  it("can login", () => {
    cy.createUser().then(user => {
      cy.visit("/")
        .getByPlaceholderText(/email/i)
        .type(user.email)
        .getByPlaceholderText(/password/i)
        .type(user.password)
        .getByText(/login/i)
        .parent()
        .click()
        .url()
        .should("eq", `${Cypress.config().baseUrl}/`)
        .window()
        .its("localStorage.auth")
        .should("be.a", "string");
    });
  });
});
