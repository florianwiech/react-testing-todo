import { todoBuilder } from "../support/generate";

describe("Todo", function() {
  it("has initially no tasks", () => {
    cy.loginNewUser().then(user => {
      cy.visit("/")
        .queryByText("/no data/i")
        .getByText(/tasks.+left/i)
        .invoke("text")
        .should("include", "0");
    });
  });
  it("can create a todo", () => {
    cy.loginNewUser().then(user => {
      const todo = todoBuilder();
      cy.visit("/")
        .getByPlaceholderText(/done/i)
        .type(todo.title)
        .type("{enter}")
        .wait(200)
        .getByPlaceholderText(/done/i)
        .should("be.empty")
        .getByText(/tasks.+left/i)
        .invoke("text")
        .should("include", "1")
        .queryByTestId("list-item")
        .should("have.length", 1);
    });
  });
  it("can edit a todo", () => {
    cy.loginNewUser().then(user => {
      cy.createTodo().then(todo => {
        cy.visit("/");
      });
    });
  });

  it("can delete a todo", () => {
    cy.loginNewUser().then(user => {
      cy.createTodo().then(todo => {
        cy.visit("/");
      });
    });
  });
});
1;
